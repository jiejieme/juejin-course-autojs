engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});

runtime.loadDex("./color.dex");
importClass(Packages.mil.nga.color.Color);
let color = new Color("#ff0000");
let hsl = color.getHSL();
log(hsl); 
