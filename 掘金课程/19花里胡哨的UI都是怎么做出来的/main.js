"ui";
engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});
ui.layout(
    <vertical bg="#2a2829">
        <list id="list">
            <horizontal w="*" h="{{this.radius*2}}px" margin="10 10 10 10">
                <card w="{{this.radius*2}}px" h="{{this.radius*2}}px" cardCornerRadius="{{this.radius}}px" cardBackgroundColor="{{this.color}}">
                    <img src="{{this.icon}}" padding="{{parseInt(this.radius/2)}}px"></img>
                </card>
                <vertical marginLeft="{{parseInt(this.radius/2)}}px" h="*" gravity="center">
                    <text textColor="#ffffff" textStyle="bold" textSize="{{parseInt(this.textSize*1.3)}}px" text="{{this.middleText.top}}"></text>
                    <text textColor="#aaffffff" textSize="{{this.textSize}}px" text="{{this.middleText.bottom}}"></text>
                </vertical>
                <text textColor="#ffffff" textSize="{{parseInt(this.textSize*1.8)}}px" text="{{this.rightText}}" h="*" w="*" gravity="right|center"></text>
            </horizontal>
        </list>
    </vertical>
);
let radius = parseInt(device.width / 12);
log(radius);
let items = [
    {
        icon: "@drawable/ic_event_black_48dp",
        middleText: {
            top: "top",
            bottom: "bottom",
        },
        rightText: "rightText",
        color: "#25e9e7",
    },
    {
        icon: "@drawable/ic_event_black_48dp",
        middleText: {
            top: "top",
            bottom: "bottom",
        },
        rightText: "rightText",
        color: "#d5b9ff",
    },
    {
        icon: "@drawable/ic_event_black_48dp",
        middleText: {
            top: "top",
            bottom: "bottom",
        },
        rightText: "rightText",
        color: "#eccaaa",
    },
];
let textSize = parseInt(radius / 2);
var len = items.length;
for (var i = 0; i < len; i++) {
    items[i].radius = radius;
    items[i].textSize = textSize;
}
log(items);
ui.list.setDataSource(items);
