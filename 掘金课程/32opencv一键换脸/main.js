runtime.images.initOpenCvIfNeeded();
importClass(org.opencv.core.Scalar);
importClass(org.opencv.core.Point);
importClass(java.util.LinkedList);
importClass(org.opencv.imgproc.Imgproc);
importClass(org.opencv.imgcodecs.Imgcodecs);
importClass(org.opencv.core.Core);
importClass(org.opencv.core.Mat);
importClass(org.opencv.core.MatOfDMatch);
importClass(org.opencv.core.MatOfKeyPoint);
importClass(org.opencv.core.MatOfRect);
importClass(org.opencv.core.Size);
importClass(android.graphics.Matrix);
importClass(org.opencv.android.Utils);
importClass(android.graphics.Bitmap);
importClass(org.opencv.objdetect.CascadeClassifier);
importClass(org.opencv.core.MatOfRect);
/* -------------------------------------------------------------------------- */
let womanImgPath = files.path("./woman.png");
let manImgPath = files.path("./man.png");
var womanImg = Imgcodecs.imread(womanImgPath, 1);
var manImg = Imgcodecs.imread(manImgPath, 1);

let mCascadeFile = new java.io.File(files.path("./lbpcascade_frontalface.xml"));
let mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());

let womanFace = detectFace(womanImg);
let manFace = detectFace(manImg);
womanImg.release();
manImg.release();
/* -------------------------------------------------------------------------- */
setTimeout(function () {
    womanImg = images.read(womanImgPath);
    manImg = images.read(manImgPath);
    let clip = images.clip(manImg, manFace.x, manFace.y, manFace.w, manFace.h);
    let newClip = images.resize(clip, [womanFace.w, womanFace.h]);
    let bitmap = android.graphics.Bitmap.createBitmap(womanImg.width, womanImg.height, android.graphics.Bitmap.Config.ARGB_8888);
    let canvas = new Canvas(bitmap);
    let paint = new Paint();
    canvas.drawBitmap(womanImg.bitmap, 0, 0, paint);
    canvas.drawBitmap(newClip.bitmap, womanFace.x, womanFace.y, paint);
    let resultImg = canvas.toImage();
    viewImg(resultImg);
    // /* -------------------------------------------------------------------------- */
    canvas = null;
    resultImg.recycle();
    womanImg.recycle();
    manImg.recycle();
    clip.recycle();
    newClip.recycle();
    bitmap.recycle();
}, 500);

/* -------------------------------------------------------------------------- */
function viewImg(img) {
    log("viewImg");
    let imgPath = "/sdcard/脚本/1.png";
    images.save(img, imgPath, "png");
    app.viewFile(imgPath);
}
// 检测人脸位置
function detectFace(img) {
    let faces = new MatOfRect();
    mJavaDetector.detectMultiScale(img, faces, 1.1, 2);
    log(faces.toArray().length);
    // 画出脸的位置
    for (let i = 0; i < faces.toArray().length; i++) {
        let face = faces.toArray()[i];
        Imgproc.rectangle(img, face.tl(), face.br(), new Scalar(0, 255, 0, 255), 3);
        // 获取左上右下坐标
        let x = face.x;
        let y = face.y;
        let w = face.width;
        let h = face.height;
        // 释放资源
        faces.release();
        return {
            x: x,
            y: y,
            w: w,
            h: h,
        };
    }
}
