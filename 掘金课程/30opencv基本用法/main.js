log(org.opencv.core.Core.VERSION);
runtime.images.initOpenCvIfNeeded();
importClass(org.opencv.core.Scalar);
importClass(org.opencv.core.Point);
importClass(java.util.LinkedList);
importClass(org.opencv.imgproc.Imgproc);
importClass(org.opencv.imgcodecs.Imgcodecs);
importClass(org.opencv.core.Core);
importClass(org.opencv.core.Mat);
importClass(org.opencv.core.MatOfDMatch);
importClass(org.opencv.core.MatOfKeyPoint);
importClass(org.opencv.core.MatOfRect);
importClass(org.opencv.core.Size);
importClass(android.graphics.Matrix);
importClass(org.opencv.android.Utils);
importClass(android.graphics.Bitmap);
/* -------------------------------------------------------------------------- */
let imgPath = files.path("./img.jpg");
var img = Imgcodecs.imread(imgPath);
log(img);
// Mat [ 214*385*CV_8UC3, isCont=true, isSubmat=false, nativeObj=0x7433df8520, dataAddr=0x740958b600 ]
img.release();
/* -------------------------------------------------------------------------- */
var img2 = images.read(imgPath);
log(img2.mat);
// Mat [ 214*385*CV_8UC4, isCont=true, isSubmat=false, nativeObj=0x7433df84c0, dataAddr=0x740962d180 ]
img2.recycle();
