"ui";
engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});
ui.layout(
    <horizontal margin="30">
        <text textSize="30sp">输入框</text>
        <input id="input" textSize="30sp" w="*"></input>
    </horizontal>
);

let storage = storages.create("ui-config");
let viewId = "input";
let view = ui[viewId];
var myTextWatcher = new android.text.TextWatcher({
    onTextChanged: function (CharSequence, start, before, count) {
        storage.put(viewId, CharSequence.toString());
    },
});
view.addTextChangedListener(myTextWatcher);

/* -------------------------------------------------------------------------- */
let inputContent = storage.get(viewId);
if (inputContent) {
    view.setText(inputContent);
}
