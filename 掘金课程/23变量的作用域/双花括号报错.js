"ui";
let str = "hello world";
ui.layout(
    <horizontal margin="30">
        <button id="btn" text="{{str}}"></button>
    </horizontal>
);
ui.btn.click(() => {
    log(ui.isUiThread());
    let str2 = "hi world";
    ui.layout(
        <horizontal margin="30">
            <text textSize="30sp" text="{{str2}}"></text>
        </horizontal>
    );
});