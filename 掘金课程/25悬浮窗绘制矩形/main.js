let window = floaty.rawWindow(<canvas id="canvas"></canvas>);
window.setTouchable(false);
/* -------------------------------------------------------------------------- */
let paint = new Paint();
paint["setColor(int)"](colors.parseColor("#4fdfff"));
paint.setStyle(Paint.Style.STROKE);
paint.setStrokeWidth(10);
/* -------------------------------------------------------------------------- */
window.canvas.on("draw", function (canvas) {
    canvas.drawRect(333, 333, 666, 666, paint);
});
setInterval(() => {}, 1000);
