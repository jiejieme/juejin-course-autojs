engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});
let bigImgPath = files.path("./big-img.jpg");
let bigImg = images.read(bigImgPath);

let config = {
    firstPoker: {
        left: 0,
        top: 1,
        right: 103,
        bottom: 210,
    },
    secondPoker: {
        left: 146,
        top: 1,
        right: 256,
        bottom: 210,
    },
};
let x = config.firstPoker.left;
let y = config.firstPoker.top;
let w = config.firstPoker.right - config.firstPoker.left;
let h = config.firstPoker.bottom - config.firstPoker.top;
let pokerRects = [];
while (x < bigImg.width) {
    let pokerRect = {
        left: x,
        top: y,
        right: x + w,
        bottom: y + h,
    };
    if (pokerRect.right > bigImg.getWidth()) {
        pokerRect.right = bigImg.getWidth();
    }
    pokerRects.push(pokerRect);
    x += config.secondPoker.left - config.firstPoker.left;
}

var len = pokerRects.length;
for (var i = 0; i < len; i++) {
    var rect = pokerRects[i];
    var img = images.clip(bigImg, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
    var filePath = files.join(files.getSdcardPath(), "脚本/扑克小图", "poker" + i + ".png");
    files.createWithDirs(filePath);
    images.save(img, filePath);
    log("保存成功: " + filePath);
    img.recycle();
}
bigImg.recycle();
