"ui";
engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});
ui.layout(
    <vertical>
        <button id="selectFile">选择文件</button>
    </vertical>
);
let imgRequestCode = 123;
ui.selectFile.click(function () {
    let intent = new Intent(Intent.ACTION_GET_CONTENT);
    intent.addCategory(Intent.CATEGORY_OPENABLE);
    intent.setType("image/*");
    activity.startActivityForResult(Intent.createChooser(intent, "选择图片"), imgRequestCode);
});

activity.getEventEmitter().on("activity_result", (requestCode, resultCode, data) => {
    if (requestCode == imgRequestCode) {
        if (resultCode == activity.RESULT_OK) {
            let uri = data.getData();
            log(uri);
            let bitmap = getBitmapFromUri(uri);
            log(bitmap);
        }
    }
});

function getBitmapFromUri(uri) {
    return android.provider.MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
}
