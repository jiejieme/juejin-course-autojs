let dw = device.width;
let dh = device.height;
let config = {
    dw: dw,
    dh: dh,
    bgColor: "#ff0000",
    ratio: {
        w: 0.8,
        h: 0.05,
    },
};
let windowWidth = dw * config.ratio.w;
let windowHeight = dh * config.ratio.h;
config.windowWidth = parseInt(windowWidth);
config.windowHeight = parseInt(windowHeight);
config.storage = storages.create("windowData");

module.exports = config;
